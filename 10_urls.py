from asyncio import Task
from typing import List, Tuple

from aiohttp import ClientSession
import asyncio


async def make_request(url: str) -> Tuple[int, str]:
    print(f'start request to {url}')
    async with ClientSession() as session:
        async with session.get(url=url) as response:
            print(f'before return request to {url}')
            return response.status, await response.text()


main_loop = asyncio.get_event_loop()
# не стал использовать цикл и прогонять из листа или из файла но вот реализации
"""
list_of_urls = ['1', '2', ...]
tasks = [asyncio.ensure_future(make_request(x)) for x in list_of_urls]
---
tasks = []
with open('some_file_with_urls', 'r') as f:
    tasks += [asyncio.ensure_future(make_request(x)) for x in f.read().split('\\n')]  # каждая строка кончается на \n
"""
tasks: List[Task] = [
    asyncio.ensure_future(make_request('https://google.com')),
    asyncio.ensure_future(make_request('https://youtube.com')),
    asyncio.ensure_future(make_request('https://facebook.com')),
    asyncio.ensure_future(make_request('https://twitter.com')),
    asyncio.ensure_future(make_request('https://instagram.com')),
    asyncio.ensure_future(make_request('https://www.netflix.com/kz-ru/')),
    asyncio.ensure_future(make_request('https://krisha.kz')),
    asyncio.ensure_future(make_request('https://kolesa.kz')),
    asyncio.ensure_future(make_request('https://github.com')),
    asyncio.ensure_future(make_request('https://about.gitlab.com/')),
]
main_loop.run_until_complete(asyncio.wait(tasks))
main_loop.close()
