```bash
pip install poetry
poetry install
poetry shell
python 10_urls.py # для вопроса про 10 урлов
pytest second.py # второе задание
```


1)- Как вы себя оцениваете по 10-шкале, если пропустить "7" и почему? Чему стоит доучится?
**4-5, за последние полгода нормально не кодил, лишь малый рефактор и код ревью некоторых частей, больший акцент был на дизайн и возможности реализации, и больше был занят задачами девопса**
- Как хорошо вы знаете ООП? Для чего нужны абстрактные классы, приватные методы и свойства?
**Исходя из предыдущего примерно на 6.
Абстрактные классы, насколько помню  от них только наследуются, и там обычно указывается как методы и свойства нужно переопределить. Приватные методы ну в питоне все равно можно переопределить значение функции или метода в классе, поэтому по конвенции в питоне используют 2 нижних подчеркивания `_` перед названием переменной чтобы показать что это приватная переменная и ее лучше не переопределять, ну насколько я помню классы по умолчанию mutable в питоне. И приватные методы и свойства затрагивают такой столб ООП как инкапсуляция, то есть сокрытие свойств и методов к которым не должно быть доступа, обычно для изменения или получения свойств используют гетеры и сетеры.**
- Опишите подробно для чего используется async await и какой вопрос он решает?
**Async  Await вообще произошел от генераторов и итераторов и оператора yield. Async await нужен для асинхронного программирования. Вообще при описании функции с помощью async мы говорим пайтон что это корутина, то есть подпрограмма, которая выполняется отдельно потоке, при вызове основной процесс останавливается и отдает управление этой корутие она в свою очередь по завершнию обратно, также можно приостановаливать корутину в процессе. Но для всего этого нужен  eventloop. Наверное самый яркий пример евентлуп реализован в js, там каждое нажатие выполняет событие создает промис которое по окончанию вернет данные в колбэк. В питоне тоже есть реалзиация но там уже используется библиотека asyncio. Вопрос обхода GIL, ну скорее открывает возможность писать асинхронно, примером можно взять какой парс или генератор, который вы с помощью async/await и eventloop, можете запускать параллельно.**
- Есть лист из 10-и URL. Как сделать 10 асинхронных запросов и получить содержимое этих URL?
 10_urls.py


2)Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.

Примеры:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]

Input: nums = [3,2,4], target = 6
Output: [1,2]

Input: nums = [3,3], target = 6
Output: [0,1]