"""
2)Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.

Примеры:
Input: nums = [2,7,11,15], target = 9
Output: [0,1]

Input: nums = [3,2,4], target = 6
Output: [1,2]

Input: nums = [3,3], target = 6
Output: [0,1]
"""
from typing import List

import pytest


class Solution:
    __slots__ = ('nums', 'target', 'solution')

    def __init__(self, nums: List[int], target: int):
        self.nums = nums
        self.target = target

    def resolve(self):
        _dict = dict()
        for i, v in enumerate(self.nums):
            if self.target - v in _dict:
                return i, _dict[self.target - v]
            _dict[v] = i


@pytest.mark.parametrize(
    "nums,target,solution",
    [
        ([11, 15, 2, 7], 9, (2, 3)),
        ([3, 2, 4], 6, (1, 2)),
        ([3, 3], 6, (0, 1))
    ]
)
def test_cases(nums, target, solution):
    assert set(Solution(nums, target).resolve()) == set(solution)

